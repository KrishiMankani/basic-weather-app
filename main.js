document.getElementById('Hide').style = "display:none"

const options = {
	method: 'GET',
	headers: {
		'X-RapidAPI-Key': '9c8bf00f1emsh010a693acd1dc00p12c788jsnd2ab31e2d168',
		'X-RapidAPI-Host': 'weather-by-api-ninjas.p.rapidapi.com'
	}
};
    document.getElementById('SearchBtn').addEventListener('click',(e)=>{
    document.getElementById('Hide').style = "display:block"
    let SearchedText = document.getElementById('SearchedText').value
    document.getElementById('CityName').textContent = SearchedText
    fetch('https://weather-by-api-ninjas.p.rapidapi.com/v1/weather?city='+SearchedText, options)
	.then(response => response.json())
	.then((response) => {
        let tempRes = document.getElementById('tempResponse')
        tempRes.textContent = (response.temp)
        let minTemp = document.getElementById('minResponse')
        minTemp.textContent = (response.min_temp)
        let maxTemp = document.getElementById('maxResponse')
        maxTemp.textContent = (response.max_temp)
        let feels_like = document.getElementById('feels_like')
        feels_like.textContent = (response.feels_like)
        let humidity = document.getElementById('humidity')
        humidity.textContent = (response.humidity)
        let cloud_pct = document.getElementById('cloud_pct')
        cloud_pct.textContent = (response.cloud_pct)
        let wind_speed = document.getElementById('wind-speed')
        wind_speed.textContent = (response.wind_speed)
        let sun_rise = document.getElementById('sun-rise')
        sun_rise.textContent = (response.sunrise)
        let sun_set = document.getElementById('sun-set')
        sun_set.textContent = (response.sunset)
        
        console.log(response)
    })
	.catch(err => console.error(err));
        e.preventDefault()
    })

